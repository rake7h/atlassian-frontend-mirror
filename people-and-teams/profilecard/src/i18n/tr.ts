/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Turkish
export default {
  'pt.profile-card.closed.account': 'Hesap silindi',
  'pt.profile-card.closed.account.has.date.a.few.months':
    'Bu kullanıcının hesabı birkaç ay önce silindi.',
  'pt.profile-card.closed.account.has.date.last.month':
    'Bu kullanıcının hesabı geçen ay silindi.',
  'pt.profile-card.closed.account.has.date.more.than.a.year':
    'Bu kullanıcının hesabı bir yıldan uzun bir süre önce silindi.',
  'pt.profile-card.closed.account.has.date.several.months':
    'Bu kullanıcının hesabı birkaç ay önce silindi.',
  'pt.profile-card.closed.account.has.date.this.month':
    'Bu kullanıcının hesabı bu ay silindi.',
  'pt.profile-card.closed.account.has.date.this.week':
    'Bu kullanıcının hesabı bu hafta silindi.',
  'pt.profile-card.closed.account.no.date': 'Bu kullanıcının hesabı silindi.',
  'pt.profile-card.disabled.account.default.name': 'Eski kullanıcı',
  'pt.profile-card.general.msg.disabled.user':
    'Artık bu kişiyle iş birliği yapamazsınız.',
  'pt.profile-card.inactive.account': 'Hesap devre dışı bırakıldı',
  'pt.profile-card.inactive.account.has.date.a.few.months':
    'Bu kullanıcının hesabı birkaç ay önce devre dışı bırakıldı.',
  'pt.profile-card.inactive.account.has.date.last.month':
    'Bu kullanıcının hesabı geçen ay devre dışı bırakıldı.',
  'pt.profile-card.inactive.account.has.date.more.than.a.year':
    'Bu kullanıcının hesabı bir yıldan uzun bir süre önce devre dışı bırakıldı.',
  'pt.profile-card.inactive.account.has.date.several.months':
    'Bu kullanıcının hesabı birkaç ay önce devre dışı bırakıldı.',
  'pt.profile-card.inactive.account.has.date.this.month':
    'Bu kullanıcının hesabı bu ay devre dışı bırakıldı.',
  'pt.profile-card.inactive.account.has.date.this.week':
    'Bu kullanıcının hesabı bu hafta devre dışı bırakıldı.',
  'pt.profile-card.inactive.account.no.date':
    'Bu kullanıcının hesabı devre dışı bırakıldı.',
};
