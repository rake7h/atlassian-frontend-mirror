# Charts

## Usage

`import Charts from '@atlaskit/charts';`

Detailed docs and example usage can be found [here](https://atlaskit.atlassian.com/packages/confluence/charts).
