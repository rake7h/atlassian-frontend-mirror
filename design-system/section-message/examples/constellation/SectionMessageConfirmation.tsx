import React from 'react';

import SectionMessage from '../../src';

export default () => (
  <SectionMessage appearance="confirmation">
    <p>File has been uploaded.</p>
  </SectionMessage>
);
