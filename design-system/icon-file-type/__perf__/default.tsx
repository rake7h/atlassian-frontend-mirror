import React from 'react';

// because icons are imported from absolute paths, this path is
// not how you will consume it.
// The path for you is `@atlaskit/icon-file-interface/glyph/audio/16`
import AudioIcon16 from '../glyph/audio/16';

export default () => <AudioIcon16 label="audioicon 16" />;
