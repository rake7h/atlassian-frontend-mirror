export { backgroundColors, textColors } from '../theme';
export type { ThemeAppearance, ThemeProps } from '../theme';
