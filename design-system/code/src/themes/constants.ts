export const CODE_FONT_SIZE = 12;
export const CODE_LINE_HEIGHT = '20px';

// css vars used to decrease font family being repeatedly declared inline
export const VAR_CODE_FONT_FAMILY = '--code-font-family';
export const VAR_CODE_FONT_FAMILY_ITALIC = `${VAR_CODE_FONT_FAMILY}-italic`;
export const VAR_CODE_LINE_NUMBER_BG_COLOR = '--line-number-bg-color';
