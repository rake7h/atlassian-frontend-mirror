export const INLINE_UNSUPPORTED_CONTENT_TEXT_ATTR_VALUE =
  'Invalid inline node content';

//============Unsupported Content Types===========
export const BLOCK_UNSUPPORTED_CONTENT = 'block';
export const INLINE_UNSUPPORTED_CONTENT = 'inline';
