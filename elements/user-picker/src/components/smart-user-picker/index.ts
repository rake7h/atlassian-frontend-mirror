export { default as SmartUserPicker, hydrateDefaultValues } from './components';
export type {
  SupportedProduct,
  RecommendationRequest,
  Props as SmartUserPickerProps,
  State as SmartUserPickerState,
  SmartProps,
} from './components';

export { setSmartUserPickerEnv } from './config';
